package ru.rencredit.jschool.kuzyushin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.RoleType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "app_role")
public class Role extends AbstractEntity {

    @ManyToOne
    private User user;

    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @Override
    public String toString() {
        return roleType.name();
    }
}
