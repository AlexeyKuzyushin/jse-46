package ru.rencredit.jschool.kuzyushin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.rencredit.jschool.kuzyushin.tm.dto.CustomUser;
import ru.rencredit.jschool.kuzyushin.tm.entity.Role;
import ru.rencredit.jschool.kuzyushin.tm.entity.User;
import ru.rencredit.jschool.kuzyushin.tm.enumeration.RoleType;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyLoginException;
import ru.rencredit.jschool.kuzyushin.tm.exception.empty.EmptyPasswordException;
import ru.rencredit.jschool.kuzyushin.tm.repository.IUserRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = userRepository.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);

        @NotNull final List<Role> userRoleTypes = user.getRoles();
        @NotNull final List<String> roles = new ArrayList<>();
        for (final Role role : userRoleTypes) roles.add(role.toString());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(username)
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    public void initUsers() {
        if(userRepository.count() > 0) return;
        create("test", "test", RoleType.USER);
        create("admin", "admin", RoleType.ADMIN);
    }

    public void create (final String login, final String password, final RoleType roleType) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(password));
        user.setRoleType(roleType);
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }
}
