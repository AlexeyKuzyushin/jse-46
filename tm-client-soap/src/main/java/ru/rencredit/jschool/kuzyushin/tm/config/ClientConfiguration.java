package ru.rencredit.jschool.kuzyushin.tm.config;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.rencredit.jschool.kuzyushin.tm.endpoint.soap.*;

@Configuration
public class ClientConfiguration {

    @Bean
    public TaskSoapEndpointService taskEndpointService() {
        return new TaskSoapEndpointService();
    }

    @Bean
    public TaskSoapEndpoint taskEndpoint(
            @NotNull final TaskSoapEndpointService taskEndpointService
    ) {
        return taskEndpointService().getTaskSoapEndpointPort();
    }

    @Bean
    public ProjectSoapEndpointService projectEndpointService() {
        return new ProjectSoapEndpointService();
    }

    @Bean
    public ProjectSoapEndpoint projectEndpoint(
            @NotNull final ProjectSoapEndpointService projectEndpointService
    ) {
        return projectEndpointService().getProjectSoapEndpointPort();
    }

    @Bean
    public AuthSoapEndpointService authSoapEndpointService() {
        return new AuthSoapEndpointService();
    }

    @Bean
    public AuthSoapEndpoint authSoapEndpoint(
            @NotNull final AuthSoapEndpointService authSoapEndpointService
    ) {
        return authSoapEndpointService().getAuthSoapEndpointPort();
    }
}
